# Frikiminutos (Informática I)

* 2023-09-26. [Asistentes IA para programar](pycharm_ia.md)
* 2023-10-03. [Servidor web Python de una línea](python_http.md)
* 2023-10-05. [Python como calculadora](python_math.md)