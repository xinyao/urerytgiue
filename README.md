# Materiales (Informática I, ISAM, URJC)

Repositorio con el fuente de los principales materiales que estamos usando en la asignatura Informática I, del Grado en Ingeniería de Sistemas Audiovisuales y Multimedia, de la Universidad Rey Juan Carlos.

## Materiales de prácticas

* [El entorno de programación I](practicas/entorno-i)
* [El entorno de programación II](practicas/entorno-ii)

## Otros materiales

* [Python snippets](frikiminutos-2022)



