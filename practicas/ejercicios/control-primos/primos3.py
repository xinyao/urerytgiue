#!/usr/bin/env pythoh3

'''
Program to compute odd numbers between two given.
 This version is more efficient than primos.py and primos2.py:
 we only need to test divisors until sqrt(num) to check if num is prime
 This version uses a function to determine if a number is prime.
'''

import math

def is_prime(number):
    """Determine if number is prime (return True) or  not (return False)"""

    # Every number is prime until we can prove otherwise
    prime = True
    for divisor in range(2, math.floor(math.sqrt(number))+1):
        if (number % divisor) == 0:
            # We found a divisor, number is not prime
            prime = False
            break;
    return prime

limit: int = int(input("Dame un número entero no negativo: "))

print("Números primos iguales o menores:", end='')

for number in range(1, limit+1):
    # Let's check if we find an exact divisor
    if is_prime (number):
        print('', number, end='')
