# El entorno de programación II

## Depuración (debugging)

Actividades:

* Explicación del depurador:
  * Puntos de parada, ejecución paso a paso
  * Variables y ámbitos
* Ejercicio con el depurador (usando por ejemplo `dni,py`)

Referencias:

* [PyCharm: Debugging Python Code](https://www.jetbrains.com/help/pycharm/part-1-debugging-python-code.html)

## Entornos virtuales (venv)

Actividades:

* Explicación del entorno virtual:
  * Creación: `python3 -m venv <dir>`
  * Activación: `<dir>/bin/activate`
  * Desactivación: `deactivate`
* Entornos virtuales en PyCharm

Referencias:

* [Python: Creation of virtual environments](https://docs.python.org/3/library/venv.html)
* [PyCharm: Configure a virtual environment](https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html)

## Instalación de paquetes (pip)

Actividades:

* Explicación de la instalación de paquetes:
  * https://pypi.org
  * `pip install <pkg>`
* Ejercicio: ejecuta [qr.py](../../frikiminutos-2022/qr.py)
  * Crea entorno virtual
  * Instala paquetes
  * Ejecuta `qr.py`
* Ejercicio: ejecuta [qr.py](../../frikiminutos-2022/qr.py) en PyCharm
  * Crea un entorno virtual en PyCharm
  * Instala paquetes
  * Ejecuta `qr.py`

Referencias:

* [Python: Installing Packages](https://packaging.python.org/en/latest/tutorials/installing-packages/)
* [PyCharm: Install, uninstall, and upgrade packages](https://www.jetbrains.com/help/pycharm/installing-uninstalling-and-upgrading-packages.html)

## Aserciones (assertions)

Ejemplos:

```python
num = int(input())
assert num > 10
assert num > 10, "num is not greater than 10"
assert num > 10, f"num ({num} is not greater than 10"
assert num > 10, \
  f"num ({num} is not greater than 10"
```

```commandline
assert val in ['A', 'B', 'C', 'D']
assert isinstance(num, int)
```

Referencias:

* [Python's assert: Debug and Test Your Code Like a Pro](https://realpython.com/python-assert-statement/)


## Pruebas (testing)

Actividades:

* Copia el directorio `ejercicios/intro-segundos-2` en un repositorio local.
* Ábrelo como un proyecto con PyCharm
* Ejecuta los tests (ejecutando el directorio `tests`)
* Ejecuta los tests (ejecutando el fichero `tests/test_segundos.py`
* Fuerza algún error, bien cambiando el código de `segundos.py`, bien cambiando alguna de las comprobaciones en los tests.

Referencias:

* [Real Python: Getting Started With Testing in Python](https://realpython.com/python-testing/)

## Análisis estático (MyPy)

Actividades:

* Inatala el plugin MyPy de PyCharm (menú `Settings`, opción `Plugins`).
* Copia el fichero `intro-segundos-3/segundos.py` en un directorio, y ábrelo como repositorio en PyCharm.
* Ejecuta MyPy para comprobarlo

Referencias:

* [PyCharm MyPy plugin](https://plugins.jetbrains.com/plugin/11086-mypy)
* [MyPy Documentation](https://mypy.readthedocs.io/en/stable/index.html)
* [How to start using Python Type Annotations with Mypy](https://www.stackbuilders.com/blog/using-types-in-python-with-mypy/)