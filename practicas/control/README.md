# Estructuras de control

## Condicionales y bucles

Actividades:

* Presentación de las tres estructuras básicas de control: secuencia, condicional y bucle.

* Ejercicio: "Número mayor"

Realiza un programa que pida un número, indique si es
mayor o menor que 10, y luego escriba desde ese
número hasta 1, decrementando de uno en uno. Por ejemplo:

```commandline
$ python3 numero_mayor.py
Dame un número entero: 11
Es mayor que 10
11
10
9
8
7
6
5
4
3
2
1
¡Hasta aquí hemos llegado!
```

Solución: [numero_mayor.py](numero_mayor.py)

* Ejercicio: "Números pequeños, medianos y grandes"

Escribe un programa que pida un número entero. Si el número está entre 1 y 5 escribirá "número pequeño", si está entre 6 y 10 (incluids) escribirá "número mediano", y si es mayor que 10 escribirá "número grade". Cuando lo haya hecho, volverá a pedir otro número, y así sucesivamente. Terminará cuando el usuario escriba un 0.

* Ejercicio: "Animales clasificados"

Escribe un programa que clasifique animales de acuerdo a dos características: grandes y pequeños por un lado, y rápidos y lentos por otro. Elige tú mismo los animales que clasificas (al menos seis, entre los que tendrá que haber al menos uno lento y pequeño, otro lento y grande, otro rápido y pequeño, y otro rápido y grande). El programa pedirá el nombre del animal, y escribirá si es grande o pequeño, y rápido o lento. Si no es uno de los animales que el programa considera, indicará "animal desconocido". Si en lugar de animal se pulsa simplemente la tecla "RETURN", el programa termina. Cuando esto haya terminado, se volverá a pedir otro nombre de animal, y así sucesivamente. La ejecución del programa será como sigue:

```shell
$ python3 animales_clasificados.py
Dime un animal: elefante
Es un animal rápido y grande
Dime otro animal: gato
No conozco a ese animal
Dime otro animal: 
¡Hasta luego""
```